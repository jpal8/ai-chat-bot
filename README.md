# AIChatBot

- Jessica Palacios

AIChatBot is an efficient ai chat bot that enables the user to explore a variety of questions through a responsive language model. By simply typing into the app the language model will respond to their most pressing questions!

## Design
This is a ai chat bot. The design of the system includes the Client and Server. The Client frontend creates the form used by the intended user and the Server backend uses OpenAi's API to communicate with the frontend and answer any questions requested by the user.


## Intended market
I am targeting a wide range of individuals as this app can answer a variety of questions and assist in many different problems.

## Functionality
An Ai chat bot that can be used to communicate and can serve as an assistant, guide, or responsive data model. It generates text responses to many questions with a human like response.

## Project Deployed
https://ai-chat-bot-liard.vercel.app/
